![picture](https://i.imgur.com/mOlYi7J.png)

Unofficial Google Duo client, Open source and multi-platform for all platforms to use.

## Version 1.0.2-2

voice and video call your friend and family just like on the web version all wrapped up into a desktop application!

![picture](https://i.imgur.com/A80GbqW.png)

Do both voice and video calls with everyone on Duo on multiple platforms.

![picture](https://i.imgur.com/fKtozRP.png)

![picture](https://i.imgur.com/fTMuqaf.png)


 ## Linux 64bit

 You can install Google Duo from the AUR for Arch/Manjaro distros.
 [Click Here](https://aur.archlinux.org/packages/googleduo-bin/)

 ### Zipped Portable File:

 ### Linux x64
 - [Download](https://gitlab.com/google-duo/binaries/1.0.2-2/-/raw/main/GoogleDuo-linux-x64.tar.gz)

 ### Linux Arm64 Beta
 - [Download](https://gitlab.com/google-duo/binaries/1.0.2-2/-/raw/main/GoogleDuo-linux-arm64.tar.gz)

 ### Linux Arm7l Beta
 - [Download](https://gitlab.com/google-duo/binaries/1.0.2-2/-/raw/main/GoogleDuo-linux-arm64.tar.gz)

 ## Windows 64bit

 ### Windows Setup:
 - [Download](https://gitlab.com/google-duo/binaries/1.0.2-2/-/raw/main/GoogleDuo-linux-arm64.tar.gz)

 ### Zipped Portable File:
 - [Download](https://gitlab.com/google-duo/binaries/1.0.2-2/-/raw/main/GoogleDuo-win32-x64.zip)

 ## Mac OS

 ###  x64:
 - [Download](https://gitlab.com/google-duo/binaries/1.0.2-2/-/raw/main/GoogleDuo-darwin-x64.zip)

 ###  Arm64:
 - [Download](https://gitlab.com/google-duo/binaries/1.0.2-2/-/raw/main/GoogleDuo-darwin-arm64.zip)

 ### Author
  * Corey Bruce
